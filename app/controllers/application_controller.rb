class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :find_current_tab

private
	def find_current_tab
		case params[:controller]
			when 'games'
				@current_tab = 2
			else
				@current_tab = 1
		end
	end

end
