class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.time :playedOn

      t.timestamps
    end
  end
end
