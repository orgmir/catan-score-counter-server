class FixGamePlayedColumnType < ActiveRecord::Migration
  def change
  	change_column :games, :played_on, :date 
  end
end
