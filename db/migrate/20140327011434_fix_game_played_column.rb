class FixGamePlayedColumn < ActiveRecord::Migration
  def change
  	rename_column :games, :playedOn, :played_on
  end
end
